<?php

function page_title () {

    echo "404 Page Not Found - Oops, you bwoke it.";

}

function page_contents() {
    ?>
    <div class="jumbotron">
        <h1>404’d!</h1>
        <p class="lead">Were you just making up names of files or what? I mean, I've seen some pretend file names in my day, but come on! It's like you're not even trying.</p>
        <p><a class="btn btn-lg btn-success" href="./" role="button">Try again.</a></p>
    </div>
    <?php
}
